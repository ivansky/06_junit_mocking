package edu.epam.izhevsk.junit;

import org.junit.Before;
import static org.mockito.AdditionalMatchers.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.*;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class PaymentControllerTest {
	
	private static Long AUTHORIZED_USER_ID = 100L;
	private static Long NOT_AUTHORIZED_USER_ID = 50L;
	
	private static Long MIN_AMOUNT = 1L;
	private static Long MAX_AMOUNT = 100L;
	
	private static Long CORRECT_AMOUNT = 50L;
	private static Long LARGE_INCORRECT_AMOUNT = 150L;
	
	@Mock private AccountService accountService;
	@Mock private DepositService depositService;
	@InjectMocks private PaymentController paymentController;
	
	@Before
	public void initialize() throws InsufficientFundsException {
		MockitoAnnotations.initMocks(this);
		when(accountService.isUserAuthenticated(AUTHORIZED_USER_ID)).thenReturn(true).thenReturn(false);
		when(depositService.deposit(or(lt(MIN_AMOUNT), gt(MAX_AMOUNT)), anyLong())).thenThrow(new InsufficientFundsException());
	}

	@Test
	public void testSuccessfullTransactionOnce() throws InsufficientFundsException {
		paymentController.deposit(CORRECT_AMOUNT, AUTHORIZED_USER_ID);
        verify(accountService, times(1)).isUserAuthenticated(AUTHORIZED_USER_ID);
	}
	
	@Test(expected = InsufficientFundsException.class)
	public void testLargeAmountException() throws InsufficientFundsException {
		depositService.deposit(LARGE_INCORRECT_AMOUNT, AUTHORIZED_USER_ID);
	}
	
	@Test(expected = SecurityException.class)
	public void testUnauthenticatedUserException() throws InsufficientFundsException, SecurityException {
		paymentController.deposit(CORRECT_AMOUNT, NOT_AUTHORIZED_USER_ID);
	}
 
}
